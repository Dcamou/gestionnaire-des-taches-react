import React, { useState } from 'react';
import '../styles/TaskList.css';

const TaskList = () => {
    const [tasks, setTasks] = useState([]);
    const [newTask, setNewTask] = useState('');

    const addTask = () => {
        if (newTask) {
            setTasks([...tasks, newTask]);
            setNewTask('');
        }
    };

    const deleteTask = (index) => {
        const updatedTasks = tasks.filter((_, i) => i !== index);
        setTasks(updatedTasks);
    };

    return (
        <div className="task-list">
            <h1>Liste des Tâches</h1>
            <input
            type="text"
            value={newTask}
            onChange={(e) => setNewTask(e.target.value)}
            placeholder="Nouvelle tâche"
            />
            <button className="add-button" onClick={addTask}>Ajouter</button>
            <ul>
                {tasks.map((task, index) => (
                <li key={index}>
                    {task}
                    <button className="delete-button" onClick={() => deleteTask(index)}>Supprimer</button>
                </li>
                ))}
            </ul>
        </div>
    );
};

export default TaskList;
